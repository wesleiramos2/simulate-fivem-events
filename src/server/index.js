const io = require('socket.io')

/**
 * 
 * @param {io.Server} server 
 */
const createFunctions = (server) => {
  const events = new Map()
  
  /**
   * Cria um evento do lado do servidor que pode ser
   * emitido pelo cliente
   * @param {String} event 
   * @param {Function} cb 
   */
  global.on = global.onNet = (event, cb) => {
    events.set(event, cb.toString())
  }

  /**
   * Emite um evento do lado do servidor
   * @param {String} event 
   * @param  {...any} args 
   */
  global.emit = (event, ...args) => {
    if (events.has(event)) {
      const cb = events.get(event)
      cb(...args)
    }
  }

  /**
   * Emite um evento do lado do cliente
   * @param {String} source 
   * @param {String} event 
   * @param  {...any} args 
   */
  global.emitNet = (event, source, ...args) => {
    const socket = server.sockets.connected[source]
    if (socket) socket.emit('emit-client-event', event, ...args)
  }

  /**
   * Emite um evento do servidor pelo client
   * @param {String} source 
   * @returns {(event: string, args: ...args) => void}
   */
  const emitServerEvent = source => (event, ...args) => {
    const code = events.get(event)
    if (typeof code !== 'string')
      return
    
    const cb = eval(code)
    cb(...args)
  }

  server.on('connection', socket => {
    const serverEmit = emitServerEvent(socket.id)
    serverEmit('playerConnecting')
    socket.on('emit-server-event', serverEmit)
    socket.on('disconnect', (reason) => emit('playerDropped', reason))
  })
}

module.exports = (port = 30120) => {
  if (!('onNet' in global)) {
    createFunctions(io.listen(port))
  }
}
