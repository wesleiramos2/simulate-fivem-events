import IO from 'socket.io-client'

/**
 * 
 * @param {SocketIOClient.Socket} io 
 */
const createFunctions = (io) => {

  const events = new Map()
  const nuicallbacks = new Set()

  window.SetNuiFocus = () => {}

  /**
   * Registra o nui callback
   * @param {String} event 
   */
  window.RegisterNuiCallbackType = (event) => {
    nuicallbacks.add(event)
  }
  
  /**
   * Simula post request para o backend do fivem
   * @param {String} url 
   * @param {String} data 
   */
  window.$ = {
    post(url, data) {
      const event = url.split('/').pop()
      emit(event, JSON.parse(data))
    }
  }

  /**
   * Simula o sendNuiMessage
   * @param {String} data 
   */
  window.SendNuiMessage = (data) => {
    const event = new CustomEvent('message')
    event.data = JSON.parse(data)
    window.dispatchEvent(event)
  }  
  
  /**
   * Cria um evento do lado do servidor
   * @param {String} event 
   * @param {Function} cb 
   */
  window.on = window.onNet = (event, cb) => {
    if (event.startsWith('__cfx_nui:')) {
      event = event.replace('__cfx_nui:', '')
      if (!nuicallbacks.has(event)) return false
    }

    events.set(event, cb)
  }

  /**
   * Emite um evento do lado do servidor
   * @param {String} event 
   * @param  {...any} args 
   */
  window.emit = (event, ...args) => {
    if (events.has(event)) {
      const cb = events.get(event)
      cb(...args)
    }
  }

  /**
   * Emite um evento do lado do cliente
   * @param {String} source 
   * @param {String} event 
   * @param  {...any} args 
   */
  window.emitNet = (event, ...args) => {
    io.emit('emit-server-event', event, ...args)
  }

  /**
   * Emite um evento do servidor pelo client
   * @param {String} source 
   * @returns {(event: string, args: ...args) => void}
   */
  const emitClientEvent = (event, ...args) => {
    const cb = events.get(event)
    if (!cb)
      return

    cb(...args)
  }

  io.on('emit-client-event', emitClientEvent)
}

export default (address = 'http://localhost:30120') => {
  return 'localStorage' in window ? createFunctions(IO.connect(address)) : null
}
