import FivemClient from '../../src/client'

FivemClient()

onNet('client-event', (...args) => {
  console.log('Client recebeu', ...args)
})

RegisterNuiCallbackType('event-nui')
on('__cfx_nui:event-nui', (...args) => {
  console.log('Evento da interface', ...args)
  SendNuiMessage(JSON.stringify({ event: 'test', args: [ 1, 2, 3 ] }))
})

emitNet('server-event', 'Testando...')